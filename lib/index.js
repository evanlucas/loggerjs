/**
 *	Module depends
 */
var colors = require('colors');

/*!
 * main logging function
 *
 * @param {String} title The filename or something else to differentiate this log from that log
 * @api private
 */
function logger(title, options) {
  title = title || ''
  this.str = '['+title+']'
  this.options = options || {};
}

/**
 * contructor
 *
 * @param {String} title
 * @api public
 */
exports = module.exports = function(title, options) {
  return new logger(title, options)
}

/**
 * Prints an error message
 *
 * @param {String|Object|Array|Number} msg Whatever needs to be logged (Accepts same as `console.log`)
 * @api public
 */
logger.prototype.error = function(msg) {
  var args = Array.prototype.slice.call(arguments)
  var e = 'ERROR'.red
  if (this.options.hasOwnProperty('error') && ('string' === typeof this.options.error)) {
    e = this.options.error
  }
  console.log.apply(null, [e, this.str.grey].concat(args))
}

/**
 * Prints an info message
 *
 * @param {String|Object|Array|Number} msg Whatever needs to be logged (Accepts same as `console.log`)
 * @api public
 */
logger.prototype.info = function(msg) {
  var args = Array.prototype.slice.call(arguments)
  var e = 'INFO'.magenta
  if (this.options.hasOwnProperty('info') && ('string' === typeof this.options.info)) {
    e = this.options.info
  }
  console.log.apply(null, [e, this.str.grey].concat(args))
}

/**
 * Prints a notice message
 *
 * @param {String|Object|Array|Number} msg Whatever needs to be logged (Accepts same as `console.log`)
 * @api public
 */
logger.prototype.notice = function(msg) {
  var args = Array.prototype.slice.call(arguments)
  var e = 'NOTICE'.cyan
  if (this.options.hasOwnProperty('notice') && ('string' === typeof this.options.notice)) {
    e = this.options.notice
  }
  console.log.apply(null, [e, this.str.grey].concat(args))
}

/**
 * Prints a warning message
 *
 * @param {String|Object|Array|Number} msg Whatever needs to be logged (Accepts same as `console.log`)
 * @api public
 */
logger.prototype.warn = function(msg) {
  var args = Array.prototype.slice.call(arguments)
  var e = 'WARN'.yellow
  if (this.options.hasOwnProperty('warn') && ('string' === typeof this.options.warn)) {
    e = this.options.warn
  }
  console.log.apply(null, [e, this.str.grey].concat(args))
}
