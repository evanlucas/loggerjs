module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    
    cafemocha: {
      main: {
        src: 'test.js',
        options: {
          ui: 'bdd',
          reporter: 'spec',
          colors: true
        }
      }
    }
  })
  
  grunt.loadNpmTasks('grunt-cafe-mocha')
  
  grunt.registerTask('default', ['cafemocha'])
}