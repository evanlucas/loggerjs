describe('Default', function() {
  var logger = require('./lib/index.js')('Logger')
  describe('Error', function() {
    it('Should print message', function() {
      logger.error('This is an error message')
    })
    
    it('Should work with multiple arguments', function() {
      logger.error('This', 'is', 'an', 'error', 'message')
    })
    
    it('Can print objects', function() {
      logger.error('An error occurred:', { test: 'value' })
    })
    
    it('Can print arrays', function() {
      logger.error('The following errors occurred: ', [{ test: 'error 1'}, { test: 'error 2'}])
    })
  })
  
  describe('Info', function() {
    it('Should print message', function() {
      logger.info('This is an info message'); 
    })
    it('Should work with multiple arguments', function() {
      logger.info('This', 'is', 'an', 'info', 'message')
    })
    it('Can print objects', function() {
      logger.info('Something happened:', { test: 'value' })
    })
    
    it('Can print arrays', function() {
      logger.info('Results: ', [{ test: 'error 1'}, { test: 'error 2'}])
    })
  })
  
  describe('Notice', function() {
    it('Should print message', function() {
      logger.notice('This is a notice');
    })
    it('Should work with multiple arguments', function() {
      logger.notice('This', 'is', 'a', 'notice', 'message')
    })
    it('Can print objects', function() {
      logger.notice('Something important happened:', { test: 'value' })
    })
    
    it('Can print arrays', function() {
      logger.notice('Results: ', [{ test: 'error 1'}, { test: 'error 2'}])
    })
  })
  
  describe('Warning', function() {
    it('Should print message', function() {
      logger.warn('This is a warning');
    })
    it('Should work with multiple arguments', function() {
      logger.warn('This', 'is', 'a', 'warning', 'message')
    })
    it('Can print objects', function() {
      logger.warn('A warning occurred:', { test: 'value' })
    })
    
    it('Can print arrays', function() {
      logger.warn('The following warnings occurred: ', [{ test: 'error 1'}, { test: 'error 2'}])
    })
  })
})

describe('Custom messages', function() {
  var logger = require('./lib')('Logger', {
    error: 'Error'.zebra.bold,
    info: 'Info'.blue.underline,
    notice: 'notice'.rainbow,
    warn: '~~WARNING~~'.yellow
  })
  describe('Error', function() {
    it('Should print message', function() {
      logger.error('This is an error message')
    })
    
    it('Should work with multiple arguments', function() {
      logger.error('This', 'is', 'an', 'error', 'message')
    })
    
    it('Can print objects', function() {
      logger.error('An error occurred:', { test: 'value' })
    })
    
    it('Can print arrays', function() {
      logger.error('The following errors occurred: ', [{ test: 'error 1'}, { test: 'error 2'}])
    })
  })
  describe('Info', function() {
    it('Should print message', function() {
      logger.info('This is an info message'); 
    })
    it('Should work with multiple arguments', function() {
      logger.info('This', 'is', 'an', 'info', 'message')
    })
    it('Can print objects', function() {
      logger.info('Something happened:', { test: 'value' })
    })
    
    it('Can print arrays', function() {
      logger.info('Results: ', [{ test: 'error 1'}, { test: 'error 2'}])
    })
  })
  
  describe('Notice', function() {
    it('Should print message', function() {
      logger.notice('This is a notice');
    })
    it('Should work with multiple arguments', function() {
      logger.notice('This', 'is', 'a', 'notice', 'message')
    })
    it('Can print objects', function() {
      logger.notice('Something important happened:', { test: 'value' })
    })
    
    it('Can print arrays', function() {
      logger.notice('Results: ', [{ test: 'error 1'}, { test: 'error 2'}])
    })
  })
  
  describe('Warning', function() {
    it('Should print message', function() {
      logger.warn('This is a warning');
    })
    it('Should work with multiple arguments', function() {
      logger.warn('This', 'is', 'a', 'warning', 'message')
    })
    it('Can print objects', function() {
      logger.warn('A warning occurred:', { test: 'value' })
    })
    
    it('Can print arrays', function() {
      logger.warn('The following warnings occurred: ', [{ test: 'error 1'}, { test: 'error 2'}])
    })
  })

})